import React, { useState } from "react";

const HigherOrderComponent = (InputComponent) => {

    const NewComponent = () => {
        const [count, setCount] = useState(0);
        const handelClick = () => {
            setCount(count + 1)
        }
        return (
            <InputComponent handelClick={handelClick} count={count} />
        )
    }
    return NewComponent;
}

export default HigherOrderComponent;