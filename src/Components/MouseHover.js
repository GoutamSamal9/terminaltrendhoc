import React from 'react';
import HigherOrderComponent from './HigherOrderComponent';

const MouseHover = ({ handelClick, count }) => {


    return (
        <h2 style={{ margin: '10px 0' }} onMouseOver={handelClick}> Hovered {count} Times </h2>
    )
}
export default HigherOrderComponent(MouseHover);