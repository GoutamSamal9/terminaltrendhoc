import React from 'react';
import HigherOrderComponent from './HigherOrderComponent';

const HandelClick = ({ handelClick, count }) => {


    return (
        <button style={{ margin: '10px 0' }} onClick={handelClick}>Clicked {count} Times</button>
    )
}
export default HigherOrderComponent(HandelClick);