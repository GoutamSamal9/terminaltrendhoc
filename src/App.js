import './App.css';
import HandelClick from './Components/HandelClick';
import MouseHover from './Components/MouseHover';

function App() {
  return (
    <div className="App">
      <h1>What is a higher order component? Create One higher order compoent which defines the proper usecase of HOC?</h1>
      <p>A pattern where  function takes a component as an argument and return new component. In below example i'm using same functionality but in different use cases. </p>
      <HandelClick />
      <MouseHover />
    </div>
  );
}

export default App;
